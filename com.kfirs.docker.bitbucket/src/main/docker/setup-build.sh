#!/usr/bin/env bash

# login to Docker
docker login --username ${DOCKER_USERNAME} --password ${DOCKER_PASSWORD}
if [[ $? != 0 ]]; then
    (>&2 echo "Failed to login to Docker!")
    exit 1
fi

# fix docker credentials to work with Docker Hub
sed -i'' 's/https:\/\/index.docker.io\/v1\//docker.io/' ~/.docker/config.json
if [[ $? != 0 ]]; then
    (>&2 echo "Failed to fix Docker credentials to work with Docker Hub!")
    exit 1
fi

# generate a Maven 'settings.xml' contents into the "SETTINGS_XML_CONTENT" bash variable
echo "Applying credentials from environment to ~/.m2/settings.xml..."
SETTINGS_XML_CONTENT=$(cat <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<settings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.1.0 http://maven.apache.org/xsd/settings-1.1.0.xsd"
          xmlns="http://maven.apache.org/SETTINGS/1.1.0">

    <!-- ============================================================================================ -->
    <!-- Everything should be mirrored through 'remote-repos' EXCEPT actual real repositories         -->
    <!-- ============================================================================================ -->
    <mirrors>
        <mirror>
            <mirrorOf>!central,!plugins-release,!plugins-snapshot,*</mirrorOf>
            <id>remote-repos</id>
            <name>Remote Repos</name>
            <url>https://repo.kfirs.com/artifactory/remote-repos</url>
        </mirror>
    </mirrors>

    <!-- ============================================================================================ -->
    <!-- Define the 'releases' and 'snapshots' profiles that define our actual repositories           -->
    <!-- ============================================================================================ -->
    <profiles>
        <profile>
            <id>releases</id>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <repositories>
                <repository>
                    <id>central</id>
                    <name>Released Artifacts</name>
                    <url>https://repo.kfirs.com/artifactory/libs-release</url>
                    <releases>
                        <enabled>true</enabled>
                        <updatePolicy>daily</updatePolicy>
                    </releases>
                    <snapshots>
                        <enabled>false</enabled>
                    </snapshots>
                </repository>
            </repositories>
            <pluginRepositories>
                <pluginRepository>
                    <id>plugins-release</id>
                    <name>Released Plugin Artifacts</name>
                    <url>https://repo.kfirs.com/artifactory/plugins-release</url>
                    <releases>
                        <enabled>true</enabled>
                        <updatePolicy>daily</updatePolicy>
                    </releases>
                    <snapshots>
                        <enabled>false</enabled>
                    </snapshots>
                </pluginRepository>
            </pluginRepositories>
        </profile>
        <profile>
            <id>snapshots</id>
            <repositories>
                <repository>
                    <id>central</id>
                    <name>Snapshot Artifacts</name>
                    <url>https://repo.kfirs.com/artifactory/libs-snapshot</url>
                    <releases>
                        <enabled>true</enabled>
                        <updatePolicy>daily</updatePolicy>
                    </releases>
                    <snapshots>
                        <enabled>true</enabled>
                        <updatePolicy>daily</updatePolicy>
                    </snapshots>
                </repository>
            </repositories>
            <pluginRepositories>
                <pluginRepository>
                    <id>plugins-snapshot</id>
                    <name>Snapshot Plugin Artifacts</name>
                    <url>https://repo.kfirs.com/artifactory/plugins-snapshot</url>
                    <releases>
                        <enabled>false</enabled>
                    </releases>
                    <snapshots>
                        <enabled>true</enabled>
                        <updatePolicy>daily</updatePolicy>
                    </snapshots>
                </pluginRepository>
            </pluginRepositories>
        </profile>
    </profiles>

    <!-- ============================================================================================ -->
    <!-- Servers section defines basically our deployment credentials                                 -->
    <!-- ============================================================================================ -->
    <servers>
        <server>
            <id>www</id>
            <username>${WWW_USER}</username>
        </server>
        <server>
            <id>central</id>
            <username>${ARTIFACTORY_USER}</username>
            <password>${ARTIFACTORY_PASS}</password>
        </server>
        <server>
            <id>plugins-release</id>
            <username>${ARTIFACTORY_USER}</username>
            <password>${ARTIFACTORY_PASS}</password>
        </server>
        <server>
            <id>plugins-snapshot</id>
            <username>${ARTIFACTORY_USER}</username>
            <password>${ARTIFACTORY_PASS}</password>
        </server>
        <server>
            <id>libs-release-local</id>
            <username>${ARTIFACTORY_USER}</username>
            <password>${ARTIFACTORY_PASS}</password>
        </server>
        <server>
            <id>libs-snapshot-local</id>
            <username>${ARTIFACTORY_USER}</username>
            <password>${ARTIFACTORY_PASS}</password>
        </server>
        <server>
            <id>plugins-release-local</id>
            <username>${ARTIFACTORY_USER}</username>
            <password>${ARTIFACTORY_PASS}</password>
        </server>
        <server>
            <id>plugins-snapshot-local</id>
            <username>${ARTIFACTORY_USER}</username>
            <password>${ARTIFACTORY_PASS}</password>
        </server>
    </servers>

    <!-- ============================================================================================ -->
    <!-- Active profiles                                                                              -->
    <!-- ============================================================================================ -->
    <activeProfiles>
        <activeProfile>releases</activeProfile>
    </activeProfiles>

    <!-- ============================================================================================ -->
    <!-- Which group IDs should Maven look inside (by default) for plugin prefixes                    -->
    <!-- ============================================================================================ -->
    <pluginGroups>
        <pluginGroup>external.atlassian.jgitflow</pluginGroup>
        <pluginGroup>org.codehaus.mojo</pluginGroup>
        <pluginGroup>org.springframework.boot</pluginGroup>
    </pluginGroups>

</settings>
EOF
)
if [[ $? != 0 ]]; then
    (>&2 echo "Failed to generate settings.xml content!")
    exit 1
fi

# save the "SETTINGS_XML_CONTENT" bash variable to "~/.m2/settings.xml"
echo ${SETTINGS_XML_CONTENT} > ~/.m2/settings.xml
if [[ $? != 0 ]]; then
    (>&2 echo "Failed to write pre-processed settings.xml content into ~/.m2/settings.xml!")
    exit 1
fi

# initialize the google cloud SDK (gcloud)
echo "${BITBUCKET_SVC_ACC_JSON}" > ~/gcloud_service_account.json
gcloud auth activate-service-account --key-file ~/gcloud_service_account.json
RC=$?
rm -f ~/gcloud_service_account.json
if [[ ${RC} != 0 ]]; then
    (>&2 echo "Failed to authorize Google Cloud SDK using our service account!")
    exit 1
fi
